To use this bot:
* use `python3.7+`
* create `.env` file and put variables to it. See `.env-local` as an example
* run `python bot.py`
