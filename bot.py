import logging
import os

import requests
import telebot
from dotenv import load_dotenv
from telebot import apihelper, types

load_dotenv()
apihelper.ENABLE_MIDDLEWARE = True

makrdown_message = """
**Привет, в этом боте вы можете отметить адреса, где уже розданы листовки.** 
Чтобы поставить отметку:

 1. Нажмите на скрепку
 2. Выберите "Геопозиция" / "Location"
 3. Поставьте на карте метку, где вы раздали материалы
 4. Нажмите "Отправить геопозицию" / "Send this location"
 5. Выберите, какую газеты вы раздали
 6. Введите номер подъезда, в котором вы распространили материалы
 7. Дождитесь сообщения "Ваша отметка сохранена"
"""


USER_DATA = {}

logger = telebot.logger
telebot.logger.setLevel(logging.INFO)

map_url = os.environ["MAP_URL"]
point_url = os.environ["POINT_URL"]
mirror_point_url = os.environ["MIRROR_POINT_URL"]
docs_url = os.environ["DOCS_URL"]
bot_key = os.environ["BOT_KEY"]
auth_token = os.environ["AUTH_TOKEN"]

use_mirror = os.getenv("USE_MIRROR", False)

bot = telebot.TeleBot(bot_key)

all_porches = ["ВСЕ", "ВСЕ ПОДЪЕЗДЫ", "ВСЕ ПОДЬЕЗДЫ"]

headers = {
    "Authorization": f"Basic {auth_token}",
    "Content-Type": "application/json;charset=utf-8",
}


def get_or_create_user_data(user_id):
    try:
        return USER_DATA[user_id]
    except KeyError:
        USER_DATA[user_id] = {"porch": 1, "document_id": None, "lat": "", "lon": ""}
        return USER_DATA[user_id]


@bot.middleware_handler(update_types=["message"])
def set_user_data(bot_instance, message):
    bot_instance.user_data = get_or_create_user_data(message.from_user.id)


@bot.message_handler(commands=["start", "help"])
def start_message(message):
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    open_maps_with_flyers = types.KeyboardButton("Показать отметки")
    how_to_bot = types.KeyboardButton("Как работать с ботом")
    markup.add(how_to_bot, open_maps_with_flyers)
    bot.send_message(
        message.chat.id, makrdown_message, parse_mode="Markdown", reply_markup=markup,
    )


@bot.message_handler(content_types=["location"])
def listen_location(message):
    result = requests.get(url=docs_url)
    newspapers = result.json()
    buttons = []
    lat = message.location.latitude
    lon = message.location.longitude
    data = {"lat": lat, "lon": lon}
    bot.user_data.update(data)

    keyboard = types.InlineKeyboardMarkup(row_width=1)
    for paper in newspapers:
        buttons.append(
            types.InlineKeyboardButton(text=paper["title"], callback_data=paper["id"])
        )
    keyboard.add(*buttons)
    bot.send_message(
        message.chat.id,
        "Выберите название газеты, которую вы раздали",
        reply_markup=keyboard,
    )


@bot.callback_query_handler(func=lambda call: True)
def process_callback(call):
    bot.user_data = get_or_create_user_data(call.from_user.id)
    bot.user_data["document_id"] = call.data
    msg = bot.send_message(call.message.chat.id, "Введите номер подъезда")
    bot.register_next_step_handler(msg, process_porch)


def process_porch(message):
    if message.text.capitalize() in all_porches:
        bot.user_data["porch"] = "Все подъезды"
    else:
        porch = message.text
        if not porch.isdigit():
            porch = 1
        bot.user_data["porch"] = porch

    requests.post(point_url, headers=headers, json=bot.user_data)
    if use_mirror:
        requests.post(
            mirror_point_url,
            data={"lat": bot.user_data["lat"], "long": bot.user_data["lon"]},
        )

    bot.send_message(message.chat.id, "Ваша отметка сохранена")


@bot.message_handler(content_types=["text"])
def show_map(message):
    if message.text == "Показать отметки":
        keyboard = types.InlineKeyboardMarkup()
        url_button = types.InlineKeyboardButton(text="Открыть карту", url=map_url)
        keyboard.add(url_button)
        bot.send_message(
            message.chat.id,
            "Нажмите на ссылку ниже, чтобы открыть карту",
            reply_markup=keyboard,
        )
    elif message.text == "Как работать с ботом":
        bot.send_message(
            message.chat.id, makrdown_message, parse_mode="Markdown",
        )


if __name__ == "__main__":
    bot.polling(none_stop=True)
